from django.test import RequestFactory, TestCase
from django.core.management import call_command
from user.models import Profile
from projects.models import TaskOffer
from projects.helpers import create_offer
import sys


class OfferCreationTestCase(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", 'seed.json', verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.joe = Profile.objects.get(pk=3)

    def test_offer_title_empty(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act
        create_offer(self, title="")

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers)

    def test_offer_title_normal(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act
        create_offer(self, title="test")

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_title_max_length(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, title="h" * 200)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_title_max_length_plus_one(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, title="h" * 201)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers)

    def test_offer_description_empty(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act
        create_offer(self, description="")

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers)

    def test_offer_description_normal(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act
        create_offer(self, description="description")

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_description_max_length(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, description="d" * 500)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_description_max_length_plus_one(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, description="d" * 501)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers)

    def test_offer_price_empty(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, price="")

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers)

    def test_offer_price_normal(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, price=10)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_price_negative(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, price=-1)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_offer_price_maximum(self):
        # arrange
        offers_before = TaskOffer.objects.count()
        # act

        create_offer(self, price=-sys.maxsize)

        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)
