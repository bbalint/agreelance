from projects.views import project_view


def create_offer(self, title="new offer", description="description", price=10):
    request = self.factory.post('/projects/1',
                                data={'title': title,
                                      'description': description,
                                      'price': price,
                                      'offer_submit': '',
                                      'taskvalue': 1})
    request.user = self.joe
    request.user.profile = self.joe
    # act
    project_view(request, 1)
