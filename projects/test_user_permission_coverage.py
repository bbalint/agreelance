from django.test import RequestFactory, TestCase
from projects.views import get_user_task_permissions
from django.core.management import call_command
from user.models import Profile
from projects.models import TaskOffer


class UserPermissionTestCase(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", 'seed.json', verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.harry = Profile.objects.get(pk=2)
        self.joe = Profile.objects.get(pk=3)

    def test_owner_user_permission(self):
        # arrange
        permissions = get_user_task_permissions(self.admin.user, TaskOffer.objects.get(pk=1).task)
        # assert
        self.assertEqual(permissions,
                         {
                             'write': True,
                             'read': True,
                             'modify': True,
                             'owner': True,
                             'upload': True,
                         })

    def test_accepted_user_permission(self):
        # arrange
        permissions = get_user_task_permissions(self.joe.user, TaskOffer.objects.get(pk=1).task)
        # assert
        self.assertEqual(permissions,
                         {
                             'write': True,
                             'read': True,
                             'modify': True,
                             'owner': False,
                             'upload': True,
                         })

    def test_not_accepted_user_permission(self):
        # arrange
        permissions = get_user_task_permissions(self.harry.user, TaskOffer.objects.get(pk=1).task)
        # assert
        self.assertEqual(permissions,
                         {
                             'write': False,
                             'read': False,
                             'modify': False,
                             'owner': False,
                             'view_task': False,
                             'upload': False
                         })
