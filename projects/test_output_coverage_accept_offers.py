from django.test import RequestFactory, TestCase
from django.core.management import call_command
from user.models import Profile
from projects.models import TaskOffer
from projects.views import project_view

projects_path = '/projects/'
project_id = '1'


class OfferAcceptanceTestCase(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", 'seed.json', verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.joe = Profile.objects.get(pk=3)

    def test_from_pending_to_accepted(self):
        # arrange
        offer_before = TaskOffer.objects.get(pk=1)
        offer_before.status = 'p'
        offer_before.save()
        self.assertEqual(offer_before.status, 'p')
        request = self.factory.post(projects_path + project_id,
                                    data={'feedback': 'Good',
                                          'offer_response': '',
                                          'status': 'a',
                                          'taskofferid': 1})
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        offer_now = TaskOffer.objects.get(pk=1)
        self.assertEqual(offer_now.status, 'a')

    def test_from_pending_to_declined(self):
        # arrange
        offer_before = TaskOffer.objects.get(pk=1)
        offer_before.status = 'p'
        offer_before.save()
        self.assertEqual(offer_before.status, 'p')
        request = self.factory.post(projects_path + project_id,
                                    data={'feedback': 'Good',
                                          'offer_response': '',
                                          'status': 'd',
                                          'taskofferid': 1})
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        offer_now = TaskOffer.objects.get(pk=1)
        self.assertEqual(offer_now.status, 'd')

    def test_from_declined_to_pending(self):
        # arrange
        offer_before = TaskOffer.objects.get(pk=1)
        offer_before.status = 'd'
        offer_before.save()
        self.assertEqual(offer_before.status, 'd')
        request = self.factory.post(projects_path + project_id,
                                    data={'feedback': 'Good',
                                          'offer_response': '',
                                          'status': 'p',
                                          'taskofferid': 1})
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        offer_now = TaskOffer.objects.get(pk=1)
        self.assertEqual(offer_now.status, 'p')

    def test_from_accepted_to_pending(self):
        # arrange
        offer_before = TaskOffer.objects.get(pk=1)
        offer_before.status = 'a'
        offer_before.save()
        self.assertEqual(offer_before.status, 'a')
        request = self.factory.post(projects_path + project_id,
                                    data={'feedback': 'Good',
                                          'offer_response': '',
                                          'status': 'p',
                                          'taskofferid': 1})
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        offer_now = TaskOffer.objects.get(pk=1)
        self.assertEqual(offer_now.status, 'p')
