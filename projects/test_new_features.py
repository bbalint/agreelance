from django.test import RequestFactory, TestCase
from django.core.management import call_command
from projects.views import projects, new_project, project_view
from user.models import Profile, Score
from projects.models import Comment

seed = 'seed.json'
projects_path = '/projects/'
project_id = '1'


class FilterFeatureTest(TestCase):

    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", seed, verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.add_project_request = self.factory.post(projects_path + 'new/', data={
            'title': 'eWLMYjmRgzAkQw3Vw6nq',
            'description': 'add new project',
            'category_id': 1,
            'task_title': 'task',
            'task_budget': 150,
            'task_description': 'do task',
        })
        self.add_project_request.user = self.admin.user

    def test_filter_in_bounds(self):
        # arrange
        # add new project with budget of 150
        new_project(self.add_project_request)
        # act
        # set the filter
        request = self.factory.get(projects_path, data={
            'min-budget': 100,
            'max-budget': 2000,
            'budget-submit': 'filter'
        })
        # assert
        # check if newly added project is rendered
        result = projects(request).getvalue()
        self.assertTrue('eWLMYjmRgzAkQw3Vw6nq'.encode('utf-8') in result)

    def test_filter_lower_bound(self):
        # arrange
        # add new project with total budget of 150
        new_project(self.add_project_request)
        # act
        # set the filter
        request = self.factory.get(projects_path, data={
            'min-budget': 180,
            'max-budget': 200,
            'budget-submit': 'filter'
        })
        # assert
        # check if newly added project is rendered
        result = projects(request).getvalue()
        self.assertFalse('eWLMYjmRgzAkQw3Vw6nq'.encode('utf-8') in result)

    def test_filter_upper_bound(self):
        # arrange
        # add new project with total budget of 150
        new_project(self.add_project_request)
        # act
        # set the filter
        request = self.factory.get(projects_path, data={
            'min-budget': 10,
            'max-budget': 100,
            'budget-submit': 'filter'
        })
        # assert
        # check if newly added project is rendered
        result = projects(request).getvalue()
        self.assertFalse('eWLMYjmRgzAkQw3Vw6nq'.encode('utf-8') in result)


class CommentFeatureTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", seed, verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.harry = Profile.objects.get(pk=2)
        self.joe = Profile.objects.get(pk=3)

    def test_comment_from_owner(self):
        # arrange
        comment_text = 'adding comment as owner'
        request = self.factory.post(projects_path + project_id,
                                    data={
                                        'comment': comment_text,
                                        'comment_submit': '',
                                    })
        comments_count_before = Comment.objects.count()
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        comments_count_now = Comment.objects.count()
        query = Comment.objects.all()
        comments_now = set()
        for comment in query:
            comments_now.add(comment.text)
        self.assertEqual(comments_count_before, comments_count_now - 1)
        self.assertTrue(comment_text in comments_now)

    def test_comment_from_participant(self):
        # arrange
        comment_text = 'adding comment as participant'
        request = self.factory.post(projects_path + project_id,
                                    data={
                                        'comment': comment_text,
                                        'comment_submit': '',
                                    })
        comments_count_before = Comment.objects.count()
        request.user = self.joe.user
        # act
        project_view(request, 1)
        # assert
        comments_count_now = Comment.objects.count()
        query = Comment.objects.all()
        comments_now = set()
        for comment in query:
            comments_now.add(comment.text)
        self.assertEqual(comments_count_before, comments_count_now - 1)
        self.assertTrue(comment_text in comments_now)

    def test_comment_from_without_access(self):
        # arrange
        comment_text = 'adding comment as without access'
        request = self.factory.post(projects_path + project_id,
                                    data={
                                        'comment': comment_text,
                                        'comment_submit': '',
                                    })
        comments_count_before = Comment.objects.count()
        request.user = self.harry.user
        # act
        resp = project_view(request, 1)
        # assert
        comments_count_now = Comment.objects.count()
        self.assertEqual(comments_count_before, comments_count_now)
        self.assertEqual(403, resp.status_code)
        self.assertTrue("You do not have comment permission".encode('utf-8') in resp.getvalue())


class EvaluateFeatureTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", seed, verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.harry = Profile.objects.get(pk=2)
        self.joe = Profile.objects.get(pk=3)

    def test_evaluate_when_finished(self):
        # arrange
        joe_scores_before = Score.objects.filter(user=self.joe)
        # pre assertion
        self.assertEqual(0, len(joe_scores_before))
        request = self.factory.post(projects_path + project_id,
                                    data={
                                        'status_change': '',
                                        'status': 'f',
                                        'score': 5
                                    })
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        joe_scores_now = Score.objects.filter(user=self.joe)
        self.assertEqual(1, joe_scores_now[0].vote_count)
        self.assertEqual(5, joe_scores_now[0].avg_score)

    def test_evaluate_when_not_finished(self):
        # arrange
        joe_scores_before = Score.objects.filter(user=self.joe)
        # pre assertion
        self.assertEqual(0, len(joe_scores_before))
        request = self.factory.post(projects_path + project_id,
                                    data={
                                        'status_change': '',
                                        'status': 'p',
                                        'score': 5
                                    })
        request.user = self.admin.user
        # act
        project_view(request, 1)
        # assert
        joe_scores_now = Score.objects.filter(user=self.joe)
        self.assertEqual(0, len(joe_scores_now))

# ------------------------------------------------------------------------------------------------
# Test for score average calculation can be found in projects.test_project_view_coverage.py file
# method by te name 'test_score_average_calculation'
# ------------------------------------------------------------------------------------------------
