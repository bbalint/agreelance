from django.test import RequestFactory, TestCase
from projects.views import project_view
from django.core.management import call_command
from user.models import Profile, Score
from projects.models import TaskOffer, Comment, Project

projects_path = '/projects/'
project_id_1 = '1'
project_id_2 = '2'


class ProjectViewTestCase(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        call_command("loaddata", 'seed.json', verbosity=0)
        self.admin = Profile.objects.get(pk=1)
        self.joe = Profile.objects.get(pk=3)

    def test_basic_request(self):
        # arrange
        request = self.factory.get(projects_path + project_id_1)
        request.user = self.admin
        # act
        project_view(request, 1)
        # assert
        self.assertEqual(1, 1)

    def test_project_not_found(self):
        # arrange

        request = self.factory.get(projects_path + project_id_2)
        request.user = self.admin

        # act
        try:
            project_view(request, 2)
            self.fail("It should throw an exception")
        # assert
        except Exception:
            pass

    def test_offer(self):
        # arrange
        offers_before = TaskOffer.objects.count()

        request = self.factory.post(projects_path + project_id_1,
                                    data={'title': 'fsd',
                                          'description': 'fsd',
                                          'price': 10,
                                          'offer_submit': '',
                                          'taskvalue': 1})
        request.user = self.joe
        request.user.profile = self.joe
        # act
        project_view(request, 1)
        # assert
        offers = TaskOffer.objects.count()
        self.assertEqual(offers_before, offers - 1)

    def test_accept_offer(self):
        # arrange
        offer = TaskOffer.objects.get(pk=1)
        offer.status = 'p'
        offer.save()

        request = self.factory.post(projects_path + project_id_1,
                                    data={'feedback': 'Good',
                                          'offer_response': '',
                                          'status': 'a',
                                          'taskofferid': 1})
        request.user = self.admin.user
        request.user.profile = self.admin

        # act
        project_view(request, 1)
        # assert
        offer = TaskOffer.objects.get(pk=1)
        self.assertEqual('a', offer.status)

    def test_finish_project(self):
        # arrange
        request = self.factory.post(projects_path + project_id_1,
                                    data={
                                        'status_change': '',
                                        'status': 'f',
                                        'score': ''
                                    })
        request.user = self.admin.user
        request.user.profile = self.admin

        # act
        project_view(request, 1)

        # assert
        project = Project.objects.get(pk=1)
        self.assertEqual('f', project.status)

    def test_score(self):
        # arrange
        request = self.factory.post(projects_path + project_id_1,
                                    data={
                                        'status_change': '',
                                        'status': 'f',
                                        'score': 3
                                    })
        request.user = self.admin.user
        request.user.profile = self.admin

        # act
        project_view(request, 1)

        # assert
        score = Score.objects.filter(user=self.joe)
        self.assertEqual(3, score[0].avg_score)

    def test_score_average_calculation(self):
        # arrange

        Score.objects.create(
            user=self.joe,
            avg_score=5,
            vote_count=1
        )

        request = self.factory.post(projects_path + project_id_1,
                                    data={
                                        'status_change': '',
                                        'status': 'f',
                                        'score': 3
                                    })
        request.user = self.admin.user

        # act
        project_view(request, 1)

        # assert
        score = Score.objects.filter(user=self.joe)
        self.assertEqual(4, score[0].avg_score)

    def test_comment(self):
        # arrange
        comments_before = Comment.objects.count()

        request = self.factory.post(projects_path + project_id_1,
                                    data={
                                        'comment': 'new comment',
                                        'comment_submit': '',
                                    })
        request.user = self.joe.user
        # act
        project_view(request, 1)
        # assert
        comments_now = Comment.objects.count()
        self.assertEqual(comments_before, comments_now - 1)
