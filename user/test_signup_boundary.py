from django.test import TestCase
from django.core.management import call_command
from user.helpers import test_factory

test_password = '7ujm%2Cki8'
email_server = '@ntnu.edu'


class SignupTest(TestCase):
    def setUp(self):
        call_command("loaddata", 'seed.json', verbosity=0)
        self.base_data = {'username': 'new_user',
                          'first_name': 'new_user_first_name',
                          'last_name': 'new_user_last_name',
                          'categories': 2,
                          'company': 'new_user_company',
                          'email': 'new@user.sample',
                          'email_confirmation': 'new@user.sample',
                          'password1': test_password,
                          'password2': test_password,
                          'phone_number': '12345678',
                          'country': 'Norway',
                          'state': 'state',
                          'city': 'city',
                          'postal_code': '243231',
                          'street_address': 'Moholt ale'}
        self.max_length_data = {'username': 'a' * 150,
                                'first_name': 'a' * 30,
                                'last_name': 'a' * 30,
                                'categories': 2,
                                'company': 'a' * 30,
                                'email': 'a' * 245 + email_server,
                                'email_confirmation': 'a' * 245 + email_server,
                                'password1': test_password,
                                'password2': test_password,
                                'phone_number': '1' * 50,
                                'country': 'a' * 50,
                                'state': 'a' * 50,
                                'city': 'a' * 50,
                                'postal_code': '1' * 50,
                                'street_address': 'a' * 50}
        self.empty_field_data = [
            ('username', ''),
            ('first_name', ''),
            ('categories', ''),
            ('email', ''),
            ('email_confirmation', ''),
            ('password1', ''),
            ('password2', ''),
            ('phone_number', ''),
            ('country', ''),
            ('state', ''),
            ('city', ''),
            ('postal_code', ''),
            ('street_address', ''),
        ]
        self.max_plus_one_data = [
            ('username', 'a' * 151),
            ('first_name', 'a' * 31),
            ('company', 'a' * 31),
            ('email', 'a' * 246 + email_server),
            ('email_confirmation', 'a' * 246 + email_server),
            ('phone_number', '1' * 51),
            ('country', 'a' * 51),
            ('state', 'a' * 51),
            ('city', 'a' * 51),
            ('postal_code', '1' * 51),
            ('street_address', 'a' * 51),
        ]

    # --------------------------- valid forms -----------------------------------
    def test_normal_form(self):
        test_factory(self, base_data=self.base_data, valid=True, status=302)

    def test_max_length(self):
        test_factory(self, base_data=self.max_length_data, value=True, status=302)

    # --------------------------- invalid forms ---------------------------------
    def test_empty(self):
        for row in self.empty_field_data:
            test_factory(self, base_data=self.base_data, field=row[0], value=row[1], valid=False)

    def test_max_length_plus_one(self):
        for row in self.max_plus_one_data:
            test_factory(self, base_data=self.base_data, field=row[0], value=row[1], valid=False)
