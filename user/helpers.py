import copy

from user.models import User
from django.urls import reverse

url = reverse('signup')


def test_factory(self, base_data, field=None, value=None, valid=1, status=200):
    data = copy.deepcopy(base_data)
    if field:
        data[field] = value
    # arrange
    users_before = User.objects.count()
    # act
    resp = self.client.post(url, data)
    # assert
    users_now = User.objects.count()
    self.assertEqual(status, resp.status_code)
    if valid:
        self.assertEqual(users_before, users_now - 1)
    else:
        self.assertEqual(users_before, users_now)
